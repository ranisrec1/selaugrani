package testcase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.ProjectMethods_Facebook;

public class TC_001_Facebook extends ProjectMethods_Facebook {

	@BeforeTest
	public  void setData()
	{
		testCaseName="Tc_002_ZoomCar";
		testCaseDesc="Create a New Lead";
		category="Smoke";
		author="Rani";
		
	}

	@Test
	public void facebook() throws InterruptedException
	{
		startApp_notifications("chrome","https://www.facebook.com");
		type(locateElement("id","email"),"9884926848");
		type(locateElement("id","pass"),"Dhya@1010");
		click(locateElement("xpath","//input[@data-testid='royal_login_button']"));
		type(locateElement("xpath","//input[@data-testid='search_input']"),"testleaf");
		click(locateElement("xpath","//button[@data-testid='facebar_search_button']"));
		//String strText=getText(locateElement("xpath","//span[text()='Pages']/following::button[text()='Like'][1]"));
		click(locateElement("xpath","//span[text()='Pages']/following::button[@type='submit'][1]"));
	    //Thread.sleep(3000);
	    String strText=getText(locateElement("xpath","//span[text()='Pages']/following::button[@type='submit'][1]"));
	    System.out.println(strText);
		verifyExactText(locateElement("xpath","//span[text()='Pages']/following::button[@type='submit'][1]"), "Liked");
		click(locateElement("xpath","//span[text()='Pages']/following::button[@type='submit'][1]"));
		click(locateElement("xpath","//span[text()='Pages']/following::div[text()='TestLeaf']"));
		Thread.sleep(1000);
		verifyTitle("TestLeaf - Home");
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
