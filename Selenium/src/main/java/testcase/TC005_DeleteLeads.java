package testcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC005_DeleteLeads extends ProjectMethods {
 
	@BeforeTest
	public  void setData()
	{
		testCaseName="TC005_DeleteLeads";
		testCaseDesc="Delete a New Lead";
		category="Smoke";
		author="Rani";
		
	}
	@Test(dependsOnMethods= {"testcase.TC003_FindLeads.findLeads"},alwaysRun=true)
	public void deleteLeads()
	{
		click(locateElement("linkText", "Leads"));
		click(locateElement("linkText", "Find Leads"));
		type(locateElement("xpath", "//input[@id='ext-gen248']"), "David");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		List<WebElement> links =  driver.findElementsByXPath("//a[text()='David']");
		System.out.println(links.size());
		
		for (WebElement ele:links)
		{
			System.out.println(ele.getText());
			ele.click();
			break;
		}
		WebElement ViewLeadCompanyName=locateElement("id","viewLead_companyName_sp");
		String strText=ViewLeadCompanyName.getText();
		String[] arrOfStr = strText.split("(", 3);
		String strID=arrOfStr[1].toString();
		System.out.println(strID);
		/*click(locateElement("linkText", "Delete"));
		verifyTitle("My Leads");
		click(locateElement("linkText", "Leads"));
		click(locateElement("linkText", "Find Leads"));
		type(locateElement("name", "id"), "10865");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		verifyPartialText(locateElement("xpath", "//div[text()='No records to display']"),"No records to display");*/
		
	}
}
