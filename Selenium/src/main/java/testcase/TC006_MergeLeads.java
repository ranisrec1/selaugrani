package testcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC006_MergeLeads extends ProjectMethods {
	@BeforeTest
	public void setData()
	{
		testCaseName="TC006_MergeLeads";
		testCaseDesc="MergeLeads";
		category="Smoke";
		author="Rani";
		
	}
	@Test
	public void mergeLeads()
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRMSFA = locateElement("linktext", "CRM/SFA");
		click(eleCRMSFA);
		click(locateElement("linktext", "Leads"));
		click(locateElement("linktext", "Merge Leads"));
		closeBrowser();
		
		
	}
}
