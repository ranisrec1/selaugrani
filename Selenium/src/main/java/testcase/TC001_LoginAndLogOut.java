package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import wdMethods.SeMethods;

@SuppressWarnings("unused")
public class TC001_LoginAndLogOut extends SeMethods{
	
	@BeforeTest
	public void setData()
	{
		testCaseName="TC001_LoginAndLogOut";
		testCaseDesc="Login and Logout";
		category="Smoke";
		author="Rani";
		
	}
	@Test
	public  void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);
		closeBrowser();
	}
	
}







