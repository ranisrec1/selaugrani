package testcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC003_FindLeads extends ProjectMethods {

	@BeforeTest
	public  void setData()
	{
		testCaseName="TC003_FindLeads";
		testCaseDesc="Edit Lead";
		category="Smoke";
		author="Rani";
		
	}
	@Test
	public void findLeads()
	{
		click(locateElement("linkText", "Leads"));
		click(locateElement("linkText", "Find Leads"));
		type(locateElement("xpath", "//input[@id='ext-gen248']"), "David");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		WebElement tbl =  locateElement("xpath", "//table[@class='x-grid3-row-table'][1]");
	    List<WebElement> tableRow = tbl.findElements(By.tagName("tr"));
		WebElement firstRow = tableRow.get(0);
		List<WebElement> tableColumn = firstRow.findElements(By.tagName("td"));
		tableColumn.get(0).findElement(By.linkText("10995")).click();
		click(locateElement("linkText", "Edit"));
		driver.findElementById("updateLeadForm_companyName").clear();
		type(locateElement("id","updateLeadForm_companyName"), "syntel");
		WebElement eleUpdate = locateElement("class", "smallSubmit");
		click(eleUpdate);
		verifyPartialText(locateElement("id", "viewLead_companyName_sp"),"syntel");
		
		
	}
	

}
