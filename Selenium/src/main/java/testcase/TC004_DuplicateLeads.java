package testcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC004_DuplicateLeads extends ProjectMethods {

	@BeforeTest
	public  void setData()
	{
		testCaseName="TC004_DuplicateLeads";
		testCaseDesc="Duplicate a Lead";
		category="Smoke";
		author="Rani";
		
	}
	@Test
	public void duplicateLeads()
	{
		click(locateElement("linkText", "Leads"));
		click(locateElement("linkText", "Find Leads"));
		type(locateElement("xpath", "//input[@id='ext-gen248']"), "David");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		List<WebElement> links =  driver.findElementsByXPath("//a[text()='David']");
		System.out.println(links.size());
		
		for (WebElement ele:links)
		{
			System.out.println(ele.getText());
			ele.click();
			break;
		}
		click(locateElement("linkText", "Duplicate Lead"));
		verifyTitle("Duplicate Lead");
		driver.findElementById("createLeadForm_firstName").clear();
		type(locateElement("id","createLeadForm_firstName"), "Ravi");
		WebElement eleUpdate = locateElement("class", "smallSubmit");
		click(eleUpdate);
		verifyExactText(locateElement("id", "viewLead_firstName_sp"),"Ravi");
		
	}
}
