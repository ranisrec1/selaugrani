package testcase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class Tc_002_ZoomCar extends ProjectMethods {
	
	@BeforeTest
	public  void setData()
	{
		testCaseName="Tc_002_ZoomCar";
		testCaseDesc="Create a New Lead";
		category="Smoke";
		author="Rani";
		
	}

	@Test
	public void zoomcar() throws InterruptedException
	{
		startApp("chrome","https://www.zoomcar.com/chennai");
		click(locateElement("linkText","Start your wonderful journey"));
		click(locateElement("xpath","//div[text()='Popular Pick-up points']/following::div[1]"));
		click(locateElement("xpath","//button[text()='Next']"));
		int tomorrow=addDates(1);
		click(locateElement("xpath","//div[contains(text(),"+"'"+tomorrow+"')]"));
		click(locateElement("xpath","//button[text()='Next']"));
		WebElement eleTime=locateElement("xpath","//div[text()='PICKUP TIME']/following::div[contains(text(),"+"'"+tomorrow+"')]");
		verifyPartialText(eleTime, String.valueOf(tomorrow));
		click(locateElement("xpath","//button[text()='Done']"));
		
		List<WebElement> lstBookedCars=driver.findElementsByXPath("//div[@class='price']");
		if (lstBookedCars.size() > 0)
		{
			reportStep("The number of cars booked"+lstBookedCars.size(), "pass");
			List<Integer> carBookPrice = new ArrayList<Integer>();
			for(WebElement eleBookedCar:lstBookedCars)
			{
				
				carBookPrice.add(Integer.parseInt(eleBookedCar.getText().replaceAll("[^0-9]", "")));
					
			}
			
			reportStep("The maximum price of the car is"+Collections.max(carBookPrice),"pass");
			String str=Collections.max(carBookPrice).toString();
			//System.out.println(str);
		    String strCar=driver.findElementByXPath("//div[contains(text(),'"+str+"') and @class='price']/..//..//..//following::div[@class='details']/h3").getText();
		    reportStep("The brand name of maximum price car is"+strCar,"pass");
		    click(locateElement("xpath","//div[text()[contains(.,'"+str+"')]]/following::button"));
		}
		else
		{
			reportStep("No car booked-something wrong", "fail");
		}
		
	
		
		
		
	}
}
