package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnHtmlReport {

	public static void main(String[] args) throws IOException {
		
		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(false);
		
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		
		ExtentTest test=extent.createTest("TC_001_CreateLead", "Create Lead in leafatps");
		test.assignCategory("SmokeTest");
		test.assignAuthor("Rani");
		test.pass("Browser Launched Successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.fail("Data demo sales manager not entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
		extent.flush();

	}

}
