package testcase_testng;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;


public class TC002_CreateLead extends ProjectMethods  {
	
	@BeforeTest(groups= {"common"})
	public  void setData()
	{
		testCaseName="TC002_CreateLead";
		testCaseDesc="Create a New Lead";
		category="Smoke";
		author="Rani";
		testCaseFileName="CreateLead";
		
	}

	@Test(groups= {"smoke"},dataProvider="fetchData")
	public void CreateLead (String company,String firstname,String lastname) {

		WebElement elecreateLead = locateElement("linkText", "Create Lead");
		click(elecreateLead);
		WebElement eleCmpName = locateElement("id", "createLeadForm_companyName");
		type(eleCmpName, company);
		type(locateElement("id", "createLeadForm_firstName"), firstname);
		type(locateElement("id", "createLeadForm_lastName"), lastname);
		type(locateElement("id", "createLeadForm_firstNameLocal"), firstname);
		type(locateElement("id", "createLeadForm_lastNameLocal"), lastname);
		type(locateElement("id", "createLeadForm_personalTitle"), "Mr.");
		selectDropDownUsingText(locateElement("id", "createLeadForm_dataSourceId"),"Employee");
		type(locateElement("id", "createLeadForm_generalProfTitle"), "Dr.");
		type(locateElement("id", "createLeadForm_annualRevenue"), "1234567");
		selectDropDownUsingIndex(locateElement("id", "createLeadForm_industryEnumId"),0);
		selectDropDownUsingText(locateElement("id", "createLeadForm_ownershipEnumId"),"Partnership");
		type(locateElement("id", "createLeadForm_sicCode"), "98876");
		type(locateElement("id", "createLeadForm_description"), "TocreateLead");
		type(locateElement("id", "createLeadForm_importantNote"), "TocreateLead");
		type(locateElement("id", "createLeadForm_lastNameLocal"), "Cruz");
		type(locateElement("id", "createLeadForm_personalTitle"), "Mr.");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").clear();
		type(locateElement("id", "createLeadForm_primaryPhoneCountryCode"), "91");
		type(locateElement("id", "createLeadForm_primaryPhoneAreaCode"), "44");
		type(locateElement("id", "createLeadForm_primaryPhoneExtension"), "78653");
		type(locateElement("id", "createLeadForm_departmentName"), "Medicine");
		selectDropDownUsingText(locateElement("id", "createLeadForm_currencyUomId"),"INR - Indian Rupee");
		type(locateElement("id", "createLeadForm_numberEmployees"), "91000");
		type(locateElement("id", "createLeadForm_tickerSymbol"), "sr");
		type(locateElement("id", "createLeadForm_primaryPhoneAskForName"), "Davi");
		type(locateElement("id", "createLeadForm_primaryWebUrl"), "http://www.cuirse.com");
		type(locateElement("id", "createLeadForm_generalToName"), "Parit");
		type(locateElement("id", "createLeadForm_generalAddress1"), "123,WallStreet");
		type(locateElement("id", "createLeadForm_generalCity"), "Chennai");
		selectDropDownUsingText(locateElement("id", "createLeadForm_generalCountryGeoId"),"India");
		selectDropDownUsingText(locateElement("id", "createLeadForm_generalStateProvinceGeoId"),"TAMILNADU");
		type(locateElement("id", "createLeadForm_generalPostalCode"), "600045");
		type(locateElement("id", "createLeadForm_generalPostalCodeExt"), "78654");
		type(locateElement("id", "createLeadForm_primaryPhoneNumber"), "9187636747");
		type(locateElement("id", "createLeadForm_primaryEmail"), "ranip@gmail.com");
		selectDropDownUsingText(locateElement("id", "createLeadForm_marketingCampaignId"),"Automobile");
		WebElement submit = locateElement("name", "submitButton");
		click(submit);
		verifyTitle("View Lead | opentaps CRM");
		

	}
	


}
