package learnJava;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class DigitsAscNoDuplicates {

	public static void main(String[] args) {

		Scanner scr=new Scanner(System.in);
		System.out.println();
		int number=scr.nextInt();
		int num1;
		Set<Integer> st=new TreeSet<Integer>();

		while(number>0)
		{
			num1=number%10;
			number=number/10;
			if (num1 % 2 !=0)
			{
				st.add(num1);

			}
		}

		System.out.println(st);



	}

}
