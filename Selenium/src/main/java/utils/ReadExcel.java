package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] readExcel(String fileName) throws IOException {


		XSSFWorkbook wBook = new XSSFWorkbook("./data/"+fileName+".xlsx");
		XSSFSheet sheet = wBook.getSheetAt(0);
		int rowNum = sheet.getLastRowNum();
		short cellNum = sheet.getRow(0).getLastCellNum();
		Object obj[][]=new Object[rowNum][cellNum];
		for (int i = 1; i <= rowNum; i++) 
		{
			for (int j = 0; j < cellNum; j++)
			{

				XSSFCell cell = sheet.getRow(i).getCell(j);
				obj[i-1][j] = cell.getStringCellValue();
				System.out.println(obj[i-1][j]);
			}
		}
		return obj;


	}

}
