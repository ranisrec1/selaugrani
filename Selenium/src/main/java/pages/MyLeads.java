package pages;

import wdMethods.ProjectMethods;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyLeads extends ProjectMethods {
	
	public MyLeads() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText="Create Lead")
	WebElement eleCreateLead;
			
	public CreateLead clickCreateLead()
	{
		click(eleCreateLead);
		return new CreateLead();
	}
	
	@FindBy(linkText="Find Leads")
	WebElement elefindleads;
	public Findleads clickfindleads() {
		click(elefindleads);
		return new Findleads();
	}

}
