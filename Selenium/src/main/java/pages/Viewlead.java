package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Viewlead extends ProjectMethods {
	public Viewlead() {
		PageFactory.initElements(driver, this);
	}

	public LoginPage verifycreateleadtext(String expectedtitle) {
		verifyTitle(expectedtitle);
		return new LoginPage();
	}
	@FindBy(linkText="Edit")
	WebElement eleEdit;

	public Editleads clickEditLead()
	{
		click(eleEdit);
		return new Editleads();
	}
	
/*	@FindBy(linkText="Edit")
	WebElement eleEdit;

	public dvouplicateleads clickDuplicateLead()
	{
		click(eleEdit);
		return new Editleads();
	}*/
	
	@FindBy(id="viewLead_companyName_sp")
	WebElement eleCompanyName;
	public void verifyCompanyName(String expectedText)
	{
		verifyPartialText(eleCompanyName, expectedText);
	}
}
