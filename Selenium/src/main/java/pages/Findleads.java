package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Findleads extends ProjectMethods{
public Findleads() {
	PageFactory.initElements(driver, this);
}
@FindBy(xpath="//input[@id='ext-gen248']")
WebElement elefirstname;
public Findleads Typefirstname(String data) {
	type(elefirstname, data);
	
	return this;
	}

@FindBy(xpath="//button[text()='Find Leads']")
WebElement eleclickbutton;
public Findleads clickfindleads() {
	click(eleclickbutton);
	
	return this;
	}



@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a[text()='TestLeaf'][1]")
WebElement eleLeadID;
public Viewlead clickLeadID() {
	click(eleLeadID);
	
	return new Viewlead();
	}


}
