package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Editleads extends ProjectMethods {
	
	public Editleads() 
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="updateLeadForm_companyName")
	WebElement eleCompanyName;
	public Editleads typeCompanyName(String data)
	{
		type(eleCompanyName, data);
		return this;
	}
	
	@FindBy(className="smallSubmit")
	WebElement eleUpdate;
	public Viewlead clickUpdate()
	{
		click(eleUpdate);
		return new Viewlead();
	}
	
	
	

}
