package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods{

	public CreateLead() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="createLeadForm_companyName")
	WebElement eleCompanyName;
	public CreateLead typeCompanyName(String data) {
			type(eleCompanyName, data);
			return this;
	}
	@FindBy(id="createLeadForm_firstName")
	WebElement eleFirstName;
	public CreateLead typeFirstName(String data) {
			type(eleFirstName, data);
			return this;
	}
	@FindBy(id="createLeadForm_lastName")
	WebElement eleLastName;
	public CreateLead typeLastName(String data) {
			type(eleLastName, data);
			return this;
	}
	@FindBy(name="submitButton")
	WebElement elecreatelead;
	public Viewlead ClickCreateLead() {
			click(elecreatelead);
			return new Viewlead();
			}
	
}
