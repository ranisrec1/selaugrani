package workout;

import java.util.Scanner;

public class OddNumbersSum {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int num1;
		int num2;
		int sum=0;
		
		Scanner scr=new Scanner(System.in);
		System.out.println("Enter Number1");
		num1=scr.nextInt();
		System.out.println("Enter Number2");
		num2=scr.nextInt();
		
		for (int i=num1 ;i<=num2;i++) 
		{
			if (i % 2 != 0)
			{
				System.out.println(i);
				sum=sum+i;
			}
		}
		System.out.println(sum);
	}

}
