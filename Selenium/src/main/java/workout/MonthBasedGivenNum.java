package workout;

import java.text.DateFormatSymbols;
import java.util.Scanner;

//MONTH BASED ON GIVEN NUMBER

public class MonthBasedGivenNum {

	public static void main(String[] args) {
		Scanner scr=new Scanner(System.in);
		System.out.println("Enter Given Number");
		int orignum=scr.nextInt();
		int num=orignum-1;
		String month = null;
		DateFormatSymbols ds=new DateFormatSymbols();
		String []mon=ds.getMonths();
		
		if (num >=0 && num<=11) 
		{
			month = mon[num];
			System.out.println("The month for given number "+orignum+"is "+month);

		}
		else
		{
			System.out.println("Enter number between 1 and 12");
		}
			
	}

}
