package workout;

import java.util.Scanner;

public class Reverse {

	public static void main(String[] args) {
		
		int temp=0;
		Scanner scr=new Scanner(System.in);
		System.out.println("Enter the number");
		int orignum=scr.nextInt();
		int num=orignum;
		int ri=0;
		int reversednum=0;
		
		while(num != 0)
		{
			ri=num%10;
			reversednum=reversednum*10+ri;
			num=num/10;
		}
		
		System.out.println("The reversed number is "+reversednum);
	}

}
