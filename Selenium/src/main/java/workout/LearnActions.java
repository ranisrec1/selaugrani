package workout;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnActions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//System.setProperty("webDriver.chrome.driver", "D://chromedriver.exe");
		ChromeDriver chr=new ChromeDriver();
		chr.manage().window().maximize();
		
		chr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		chr.get("http://jqueryui.com/draggable/");
		WebElement objFrame=chr.findElementByClassName("demo-frame");
		chr.switchTo().frame(objFrame);
		WebElement objWeb= chr.findElementById("draggable");
		Point x=objWeb.getLocation();
		int xOffset=x.x;
		int yOffset=x.y;
		
		Actions builder=new Actions(chr);
        builder.dragAndDropBy(objWeb, xOffset+100, yOffset+100).perform();
	}

}
