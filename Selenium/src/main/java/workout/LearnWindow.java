package workout;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindow {
	public static void main(String[] args) throws IOException 
	{
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//System.setProperty("webDriver.chrome.driver", "D://chromedriver.exe");
		ChromeDriver chr=new ChromeDriver();
		chr.manage().window().maximize();
		
		chr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		chr.get("https://www.irctc.co.in/nget/train-search");
		chr.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		chr.findElementByLinkText("Contact Us").click();
		Set<String> WindowHandle=new LinkedHashSet<>();
		WindowHandle=chr.getWindowHandles();
		List<String> lst=new ArrayList<>();
		lst.addAll(WindowHandle);
		chr.switchTo().window(lst.get(1));
		System.out.println(chr.getTitle());
		System.out.println(chr.getCurrentUrl());
		File scr=chr.getScreenshotAs(OutputType.FILE);
		File desc=new File("./snaps/irctc.png");
		FileUtils.copyFile(scr, desc);
		chr.switchTo().window(lst.get(0));
		
		chr.close();
		
	}
}
