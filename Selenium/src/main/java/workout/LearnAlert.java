package workout;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlert {

	public static void main(String[] args) 
	{
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//System.setProperty("webDriver.chrome.driver", "D://chromedriver.exe");
		ChromeDriver chr=new ChromeDriver();
		chr.manage().window().maximize();
		//SET TIME OUT
		chr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		chr.get("https://www.w3schools.com/jsref/met_win_prompt.asp");
		chr.findElementByLinkText("Try it Yourself �").click();
		Set<String> str1=new LinkedHashSet<>();
		str1=chr.getWindowHandles();
		List<String> lst=new ArrayList<>();
		lst.addAll(str1);
		chr.switchTo().window(lst.get(1));
		chr.switchTo().frame("iframeResult");
		chr.findElementByXPath("//button[text()='Try it']").click();
		chr.switchTo().alert();
		chr.switchTo().alert().sendKeys("Rani Perumal");
		chr.switchTo().alert().accept();
		String str=chr.findElementById("demo").getText();
		System.out.println(str);
		chr.switchTo().defaultContent();
		chr.close();
	}
}
