package workout;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class SeleniumProjectCreateLead {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver chr=new ChromeDriver();
		chr.manage().window().maximize();
		chr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		chr.get("http://leaftaps.com/opentaps/control/main");
		chr.findElementById("username").sendKeys("DemoSalesManager");
		chr.findElementById("password").sendKeys("crmsfa");
		chr.findElementByClassName("decorativeSubmit").click();
		chr.findElementByLinkText("CRM/SFA").click();
		chr.findElementByLinkText("Create Lead").click();
		chr.findElementById("createLeadForm_companyName").sendKeys("Syntel");
		chr.findElementById("createLeadForm_firstName").sendKeys("David");
		chr.findElementById("createLeadForm_lastName").sendKeys("Cruz");
		chr.findElementById("createLeadForm_firstNameLocal").sendKeys("David");
		chr.findElementById("createLeadForm_lastNameLocal").sendKeys("Cruz");
		chr.findElementById("createLeadForm_personalTitle").sendKeys("Mr.");
		WebElement src=chr.findElementById("createLeadForm_dataSourceId");
		Select source=new Select(src);
		source.selectByVisibleText("Employee");
		chr.findElementById("createLeadForm_generalProfTitle").sendKeys("Dr");
		chr.findElementById("createLeadForm_annualRevenue").sendKeys("1234567");
		WebElement indsrc=chr.findElementById("createLeadForm_industryEnumId");
		Select industry=new Select(indsrc);
		industry.selectByIndex(0);
		WebElement ownsrc=chr.findElementById("createLeadForm_ownershipEnumId");
		Select ownership=new Select(ownsrc);
		ownership.selectByVisibleText("Partnership");
		
		
		chr.findElementById("createLeadForm_sicCode").sendKeys("98876");
		chr.findElementById("createLeadForm_description").sendKeys("TocreateLead");
		chr.findElementById("createLeadForm_importantNote").sendKeys("TocreateLead");
		chr.findElementById("createLeadForm_primaryPhoneCountryCode").clear();
		chr.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("91");
		chr.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("44");
		chr.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("786543");
		chr.findElementById("createLeadForm_departmentName").sendKeys("Medicine");
		
		WebElement currSrc=chr.findElementById("createLeadForm_currencyUomId");
		Select currency=new Select(currSrc);
		currency.selectByVisibleText("INR - Indian Rupee");
		
		chr.findElementById("createLeadForm_numberEmployees").sendKeys("91000");
		chr.findElementById("createLeadForm_tickerSymbol").sendKeys("sr");
		chr.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Davi");
		chr.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://www.cuirse.com");
		
		chr.findElementById("createLeadForm_generalToName").sendKeys("Parit");
		chr.findElementById("createLeadForm_generalAddress1").sendKeys("123,WallStreet");
		chr.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		
		WebElement coutrySrc=chr.findElementById("createLeadForm_generalCountryGeoId");
		Select country=new Select(coutrySrc);
		country.selectByVisibleText("India");
		WebElement StateSrc=chr.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select State=new Select(StateSrc);
		State.selectByVisibleText("TAMILNADU");
		
		chr.findElementById("createLeadForm_generalPostalCode").sendKeys("600045");
		chr.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("78654");
		chr.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9187636747");
		chr.findElementById("createLeadForm_primaryEmail").sendKeys("ranip@gmail.com");
		
		WebElement mktSrc=chr.findElementById("createLeadForm_marketingCampaignId");
		Select mktSourc=new Select(mktSrc);
		mktSourc.selectByVisibleText("Automobile");
		chr.findElementByName("submitButton").click();
		String pgTitle="View Lead | opentaps CRM";
		Assert.assertEquals(chr.getTitle(), pgTitle);
		System.out.println("CreateLead Passed");
	
	}

}
