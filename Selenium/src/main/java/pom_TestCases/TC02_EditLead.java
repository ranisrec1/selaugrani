package pom_TestCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC02_EditLead extends ProjectMethods {
	
	@BeforeTest()
	public  void setData()
	{
		testCaseName="TC002_EditLead";
		testCaseDesc="Edit a New Lead";
		category="Smoke";
		author="Rani";
		testCaseFileName="EditLead";
		
	}

	@Test(dataProvider="fetchData")

	public void EditLead(String UserName,String Password,String FName,String CompanyName,String ExpectedText)
	{
		new LoginPage()
		.typeUserName(UserName)
		.typePassword(Password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickfindleads()
		.Typefirstname(FName)
		.clickfindleads()
		.clickLeadID()
		.clickEditLead()
		.typeCompanyName(CompanyName)
		.clickUpdate()
		.verifyCompanyName(ExpectedText);
		
	}

}
