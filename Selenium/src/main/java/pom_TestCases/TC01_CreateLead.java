package pom_TestCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC01_CreateLead extends ProjectMethods  {
	
	@BeforeTest()
	public  void setData()
	{
		testCaseName="TC002_CreateLead";
		testCaseDesc="Create a New Lead";
		category="Smoke";
		author="Rani";
		testCaseFileName="CreateLead";
		
	}

	@Test(dataProvider="fetchData")

	public void createLead(String UserName,String Password,String FName,String LName,String CompanyName,String ExpectedTitle)
	{
		new LoginPage().typeUserName(UserName)
		.typePassword(Password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.typeFirstName(FName)
		.typeLastName(LName)
		.typeCompanyName(CompanyName)
		.ClickCreateLead()
		.verifycreateleadtext(ExpectedTitle);
	}
}
