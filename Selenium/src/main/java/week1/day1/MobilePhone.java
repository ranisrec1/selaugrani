package week1.day1;

public class MobilePhone {

	
	public String modelNumber;
	public int totalCount;
	
	//Method,Variable,constructor,Blocks
	
	//Syntax
	//AccessModifer returnType MethodName(Argument)
	public void dialCaller() {
		
	}
	
	public void sendSMS(int num) {
		System.out.println(num+" : Good Morning");
	}
	
	public void addContact(String name,int number) {
		System.out.println(name+"-contact added for mobile number -"+number);
		
	}
	
	public boolean deleteContact(String name) {
		return false;
	}
	 
	public int getGroupCount() {
		return 20;
	}
	
	
	
}







