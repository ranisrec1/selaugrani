package week1.day1;

import java.util.Scanner;

public class PrimeNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int num1;
		int num2;
		
		Scanner scr=new Scanner(System.in);
		System.out.println("Enter Number1");
		num1=scr.nextInt();
		System.out.println("Enter Number2");
		num2=scr.nextInt();
		for (int i=num1;i<=num2;i++)
		{
			int flag=0;
			for (int j=2;j<i;j++)
			{
				if (i % j == 0)
				{
						flag=1;
						break;
				}
			}
			if (flag==0)
			{
				System.out.println(i+" is prime");
			}
		}

	}

}
