package week1.day1;

import java.util.Scanner;

public class LearnForLoops {
//Conditional statements learning
	public static void main (String[] args)
	{
		int number1=43;
		int number2=75;
		int number3=number1*number2;
		System.out.println(number3);
		
		// Scanner to read inputs
		
		Scanner scr=new Scanner(System.in);
		System.out.println("Enter num1");
		int num1=scr.nextInt();
		System.out.println("Enter num2");
		int num2=scr.nextInt();
		System.out.println("Enter num3");
		int num3=scr.nextInt();
		
		
		if ((num1>num2) && (num1>num3))
		{
			System.out.println(num1+" is greatest");
		}
		else if ((num2>num1) && (num2>num3))
		{
			System.out.println(num2+" is greatest");
		}
		else
		{
			System.out.println(num3+" is greatest");
		}
		
	}
}
